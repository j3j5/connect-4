<?php

declare(strict_types=1);

namespace Tests\Unit;

use App\Enums\Color;
use App\Exceptions\PlayerException;
use App\Player;
use Tests\TestCase;

class PlayerTest extends TestCase
{
    public function test_a_player_with_string_color_can_be_created()
    {
        $player = new Player('blue');
        $this->assertFalse($player->automated);
        $this->assertEquals(Color::BLUE, $player->color);
    }

    public function test_a_player_cannot_be_created_for_unavailable_color()
    {
        $this->expectException(PlayerException::class);
        $player = new Player('grey');
    }
}