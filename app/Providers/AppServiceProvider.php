<?php

declare(strict_types=1);

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        // Disable logging when running from Phar, see https://laravel-zero.com/docs/logging#note-on-phar-build
        config(['logging.channels.single.path' => (bool) \Phar::running()
            ? dirname(\Phar::running(false)) . '/dev/null'
            : storage_path('logs/laravel.log'),
        ]);
    }

    /**
     * Register any application services.
     */
    public function register(): void
    {
        //
    }
}
