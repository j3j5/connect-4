<?php

declare(strict_types=1);

namespace App;

use App\Enums\Color;
use App\Exceptions\GameException;
use App\Exceptions\PlayerException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Validation\ValidationException;

use function Termwind\ask;
use function Termwind\render;

class Player
{
    public readonly Color $color;
    public readonly bool $automated;

    private int $thinkingTimeMicroSeconds = 750000;

    /**
     * Constructor
     *
     * @param string|\App\Enums\Color $color Which color the player will use
     * @param bool $automated Whether is a human player or an automated one
     * @throws \App\Exceptions\PlayerException
     * @return void
     */
    public function __construct(string|Color $color, bool $automated = false)
    {
        if (is_string($color)) {
            $colorEnum = Color::tryFrom($color);
            if (!$colorEnum instanceof Color) {
                throw new PlayerException($color . ' is not a valid color');
            }
            $color = $colorEnum;
        }
        $this->color = $color;
        $this->automated = $automated;
    }

    public function play(Board $board) : Board
    {
        if ($this->automated) {
            return $this->automatedPlay($board);
        }
        return $this->askPlayerToPlay($board);
    }

    private function automatedPlay(Board $board) : Board
    {
        usleep($this->thinkingTimeMicroSeconds);
        // Random play
        do {
            $column = random_int(0, $board->columns - 1);
        } while (in_array($column, $board->getFullColumns(), true));
        return $board->dropChecker($this->color, $column);
    }

    private function askPlayerToPlay(Board $board) : Board
    {
        $column = $this->ask("Player {$this->color->value}, select a column", $this->color->value);

        try {

            $this->validateInput(
                column: $column,
                board: $board,
            );
        } catch (ValidationException $e) {
            foreach($e->errors() as $message) {
                $this->error($message);
            }
            return $this->askPlayerToPlay($board);
        }

        try {
            return $board->dropChecker($this->color, (int) $column);
        } catch (GameException $e) {
            $this->error($e->getMessage());
            return $this->askPlayerToPlay($board);
        }
    }

    /**
     *
     * @param \App\Board $board
     * @param string $column
     * @throws \Illuminate\Validation\ValidationException
     * @return void
     */
    private function validateInput(Board $board, string $column) : void
    {
        $min = 0;
        $max = $board->columns - 1;
        $rules = [
            'column' => [
                'required',
                'numeric',
                "min:$min",
                "max:$max",
                Rule::notIn($board->getFullColumns()),
            ],
        ];

        $messages = [
            'required' => "You must introduce a column between $min and $max",
            'numeric' => "You must introduce a column between $min and $max",
            'min' => "You must introduce a column between $min and $max",
            'max' => "You must introduce a column between $min and $max",
            'not_in' => 'That column is full, choose a different one',
        ];

        $validator = Validator::make(
            data: compact('column'),
            rules: $rules,
            messages: $messages
        )->validate();
    }

    private function ask(string $message, string $bgcolor) : string
    {
        $question = <<<HTML
            <span class="mt-1 ml-2 mr-1 mb-1 bg-{$bgcolor} px-1 text-black">
                $message
            </span>
        HTML;
        /** @phpstan-ignore-next-line */
        return (string) ask($question);
    }

    private function error(string $message) : void
    {
        render(<<<HTML
            <span class="mt-1 ml-2 mr-1 mb-1 bg-red px-1 text-white">
                {$message}
            </span>
        HTML);
    }
}
