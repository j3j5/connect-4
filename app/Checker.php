<?php

declare(strict_types=1);

namespace App;

use App\Enums\Color;

class Checker
{
    public readonly Color $color;
    /** @var array<int, string> */
    private array $class = [];

    /** */
    public function __construct(Color $color)
    {
        $this->color = $color;
    }

    public function addClass(string $class) : self
    {
        $this->class[] = $class;
        return $this;
    }

    public function __toString() : string
    {
        return <<<HTML
            <span class="text-{$this->color->value}
        HTML . ' ' . implode(' ', $this->class) . '">Ⓞ</span>';
    }
}
