<?php

declare(strict_types=1);

namespace App\Enums;

enum Direction
{
    case TOP_LEFT;
    case TOP;
    case TOP_RIGHT;
    case RIGHT;
    case BOTTOM_RIGHT;
    case BOTTOM;
    case BOTTOM_LEFT;
    case LEFT;

    /**
     *
     * @param int $row
     * @param int $column
     * @return array<int, int>
     */
    public function getNewCoordinates(int $row, int $column): array
    {
        return match($this) {
            Direction::TOP_LEFT => [$row - 1, $column - 1],
            Direction::TOP => [$row - 1, $column],
            Direction::TOP_RIGHT => [$row - 1, $column + 1],
            Direction::RIGHT => [$row, $column + 1],
            Direction::BOTTOM_RIGHT => [$row + 1, $column + 1],
            Direction::BOTTOM => [$row + 1, $column],
            Direction::BOTTOM_LEFT => [$row + 1, $column - 1],
            Direction::LEFT => [$row, $column - 1],
        };
    }
}
