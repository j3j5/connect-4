<?php

declare(strict_types=1);

namespace App\Commands;

use App\Board;
use App\Enums\Color;
use App\Player;
use Illuminate\Support\Arr;
use LaravelZero\Framework\Commands\Command;

use function Termwind\render;
use function Termwind\terminal;

class StartGame extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'start';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Start a game';

    protected Board $board;

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle() : int
    {
        $rows = $this->ask('How many rows?', '6');
        $columns = $this->ask('How many columns?', '7');
        $rowSizeToWin = $this->ask('How many checkers in a row to win?', '4');
        $type = $this->choice('Who should be playing?', ['Humans', 'Computer'], 'Computer');

        $this->board = app(Board::class, compact('rows', 'columns', 'rowSizeToWin'));

        $players = [];
        foreach (Arr::shuffle(Color::cases()) as $color) {
            $players[] = new Player($color, $type === 'Computer');
        }
        return $this->startGame($players);
    }

    /**
     * @param array<int, \App\Player> $players
     * @return int
     */
    private function startGame(array $players) : int
    {
        $this->render();

        while(count($this->board->getFullColumns()) < $this->board->columns) {
            foreach ($players as $player) {
                $this->board = $player->play($this->board);
                $this->render();

                $winner = $this->board->isThereWinner();
                if ($winner instanceof Color) {
                    // Highlight the winning row
                    $this->render();
                    $this->info('Player ' . $winner->value . ' wins!!');
                    return Command::SUCCESS;
                }
            }
        }

        $this->error('No space left! :(');

        return Command::FAILURE;
    }

    private function render(): void
    {
        terminal()->clear();
        render((string) $this->board->render());
    }
}
