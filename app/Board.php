<?php

declare(strict_types=1);

namespace App;

use App\Enums\Color;
use App\Enums\Direction;
use App\Exceptions\BoardException;
use App\Exceptions\GameException;
use Illuminate\Support\Arr;
use Illuminate\View\View;

class Board
{
    public readonly int $rows;
    public readonly int $columns;
    public readonly int $rowSizeToWin;

    private Color $winner;
    /** @var array<int, array<int, int>> */
    private array $winningCoordinates = [];

    /**
     *
     * @var array<int, array<int, null|\App\Checker>>
     */
    private array $grid;

    /**
     * @throws \App\Exceptions\GameException
     */
    public function __construct(int $rows, int $columns, int $rowSizeToWin)
    {
        if ($rows < 2) {
            throw new BoardException('You need at least 2 rows to play');
        }

        if ($columns < 2) {
            throw new BoardException('You need at least 2 columns to play');
        }

        $this->rows = $rows;
        $this->columns = $columns;
        $this->rowSizeToWin = $rowSizeToWin;

        // Create an empty grid
        for($row = 0; $row < $this->rows; $row++) {
            for($column = 0; $column < $this->columns; $column++) {
                $this->grid[$row][$column] = null;
            }
        }
    }

    /**
     * Drop a new checker into the board
     *
     * @throws \App\Exceptions\GameException
     */
    public function dropChecker(Color $color, int $column): self
    {
        $checker = app(Checker::class, compact('color'));
        for ($row = $this->rows - 1; $row >= 0; $row--) {
            if ($this->grid[$row][$column] === null) {
                $this->grid[$row][$column] = $checker;
                return $this;
            }
        }
        throw new GameException("Can't add another checker on column $column");
    }

    /**
     * Check whether there's a winner with the current checkers
     * on the board
     *
     * @return Color|false
     */
    public function isThereWinner(): Color|false
    {

        $winner = $this->checkVerticalRows();
        if ($winner instanceof Color) {
            return $winner;
        }

        $winner = $this->checkHorizontalRows();
        if ($winner instanceof Color) {
            return $winner;
        }

        // Check diagonals
        $winner = $this->checkDiagonalRows();
        if ($winner instanceof Color) {
            return $winner;
        }

        return false;
    }

    /**
     * Get current grid
     *
     * @return array<int, array<int, ?\App\Checker>>
     */
    public function getGrid(): array
    {
        return $this->grid;
    }

    /**
     * Get an array with columns (by index) that don't accept any more checkers
     *
     * @return array<int, int>
     */
    public function getFullColumns(): array
    {
        $fullColumns = [];
        for($column = 0; $column < $this->columns; $column++) {
            if ($this->grid[0][$column] instanceof Checker) {
                $fullColumns[] = $column;
            }
        }
        return $fullColumns;
    }

    /**
     *
     * @return array<int, array<int, int>>
     */
    public function getWinningCoordinates() : array
    {
        if (isset($this->winner)) {
            return $this->winningCoordinates;
        }
        return [];
    }

    /**
     * Render the view to show the board
     */
    public function render(): View
    {
        return view('board', ['board' => $this]);
    }

    private function checkVerticalRows(): ?Color
    {
        for ($column = 0; $column < $this->columns; $column++) {
            for($row = $this->rows - 1; $row >= 0; $row--) {
                $counter = 1;
                while($this->checkIfCheckersMatch($row, $column, Direction::TOP)) {
                    $counter++;
                    $row--;
                    if ($counter === $this->rowSizeToWin) {
                        $checker = Arr::get($this->grid, "$row.$column");
                        if ($checker instanceof Checker) {
                            $this->winner = $checker->color;
                            return $this->winner;
                        }
                    }
                }
            }
        }

        return null;
    }

    private function checkHorizontalRows(): ?Color
    {
        // Start on the last row and check all of them
        for ($row = $this->rows - 1; $row >= 0; $row--) {
            for ($column = 0; $column < $this->columns; $column++) {
                $counter = 1;
                while($this->checkIfCheckersMatch($row, $column, Direction::RIGHT)) {
                    $counter++;
                    $column++;
                    if ($counter === $this->rowSizeToWin) {
                        $checker = Arr::get($this->grid, "$row.$column");
                        if ($checker instanceof Checker) {
                            $this->winner = $checker->color;
                            return $this->winner;
                        }
                    }
                }
            }
        }
        return null;
    }

    private function checkDiagonalRows(): ?Color
    {
        // Check bottom left to top right
        $direction = Direction::TOP_RIGHT;
        // Check all diagonals starting from the first column down
        for ($row = 0; $row < $this->rows; $row++) {
            for($column = 0; $column < $this->columns; $column++) {
                $winner = $this->checkDiagonalRow($row, $column, $direction);
                if ($winner instanceof Color) {
                    $this->winner = $winner;
                    return $this->winner;
                }
            }
        }
        // Check all diagonals, starting from the last row, to the right
        for ($column = 0; $column < $this->columns; $column++) {
            for($row = $this->rows - 1; $row >= 0; $row--) {
                $winner = $this->checkDiagonalRow($row, $column, $direction);
                if ($winner instanceof Color) {
                    $this->winner = $winner;
                    return $this->winner;
                }
            }
        }

        // Check top left to bottom right
        $direction = Direction::BOTTOM_RIGHT;
        // Check all diagonals, starting from the last row, to the right
        for ($column = $this->columns - 1; $column >= 0; $column--) {
            for ($row = 0; $row < $this->rows; $row++) {
                $winner = $this->checkDiagonalRow($row, $column, $direction);
                if ($winner instanceof Color) {
                    $this->winner = $winner;
                    return $this->winner;
                }
            }
        }
        for ($row = 0; $row < $this->rows; $row++) {
            for ($column = 0; $column < $this->columns; $column++) {
                $winner = $this->checkDiagonalRow($row, $column, $direction);
                if ($winner instanceof Color) {
                    $this->winner = $winner;
                    return $this->winner;
                }
            }
        }

        // No winner found
        return null;
    }

    private function checkDiagonalRow(int $row, int $column, Direction $direction) : ?Color
    {
        $offsetX = $offsetY = 0;
        $counter = 1;

        while($this->checkIfCheckersMatch(
            row: ($row + $offsetY),
            column: ($column + $offsetX),
            direction: $direction
        )) {
            $counter++;
            if ($counter === $this->rowSizeToWin) {
                $checker = Arr::get($this->grid, ($row + $offsetY) . '.' . ($column + $offsetX));
                if ($checker instanceof Checker) {
                    return $checker->color;
                }
            }
            [$nextRow, $nextColumn] = $direction->getNewCoordinates(
                row: $row + $offsetY,
                column: $column + $offsetX
            );
            $offsetX = $nextColumn - $column;
            $offsetY = $nextRow - $row;
        }

        return null;
    }

    private function checkIfCheckersMatch(int $row, int $column, Direction $direction): bool
    {
        // If no checker at current position, not worth checking further
        $checker1 = Arr::get($this->grid, "$row.$column");
        if (!$checker1 instanceof Checker) {
            $this->winningCoordinates = [];
            return false;
        }

        [$row2, $column2] = $direction->getNewCoordinates($row, $column);
        $checker2 = Arr::get($this->grid, "$row2.$column2");
        if (!$checker2 instanceof Checker) {
            $this->winningCoordinates = [];
            return false;
        }

        $match = $checker1->color === $checker2->color;
        if ($match) {
            // Store the winning coordinates
            $this->addCoordinatesToWinning($row, $column);
            $this->addCoordinatesToWinning($row2, $column2);
        } else {
            $this->winningCoordinates = [];
        }
        return $match;
    }

    private function addCoordinatesToWinning(int $row, int $column) : void
    {
        if (!in_array([$row, $column], $this->winningCoordinates, true)) {
            $this->winningCoordinates[] = [$row, $column];
        }
    }
}
