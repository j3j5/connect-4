# Connect 4

This is a little project to make Connect-4 (the famous board game) as a CLI app using Laravel Zero and Termwind.

## Usage
You can just run `php connect-4 start` and follow the instructions to play a game.