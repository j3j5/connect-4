<div class="flex justify-center w-full">
    <div class="w-full flex justify-center">
        <div class=" mb-4 p-1 w-full text-center text-black">
            <p class="font-bold w-full text-center ">
                Connect {{ $board->rowSizeToWin }}
            </p>
            <p class="italic w-full text-center ">
                Have fun!
            </p>
        </div>
    </div>
    <div class="bg-green-600 w-full flex justify-center">
        <div class="text-center">
        <table class="">
            <thead>
            @foreach ($board->getGrid() as $row)
                <tr>
                    <th></th>
                @foreach ($row as $j => $column)
                    <th class="text-center p-1" >
                    @unless (in_array($j, $board->getFullColumns()))
                    {{ $j }}
                    @else
                    X
                    @endunless
                    </th>
                @endforeach
                </tr>
            @endforeach
            </thead>
            <tbody>
            @foreach ($board->getGrid() as $i => $row)
                <tr>
                    <td class="text-center p-1"> {{ $i }}
                    </td>
                @foreach ($row as $j => $checker)
                    <td class="p-1 @if(in_array([$i, $j], $board->getWinningCoordinates())) bg-lime-300 @elseif($checker) bg-slate-200 @endif">
                        @if(is_null($checker))
                        <span class="text-black">O</span>
                        @else
                            @php
                            if(in_array([$i, $j], $board->getWinningCoordinates())) {
                                $checker->addClass("bg-lime-300");
                            }
                            @endphp
                        {{ $checker }}
                        @endif
                    </td>
                @endforeach
                </tr>
            @endforeach
            </tbody>
        </table>
        </div>
    </div>
</div>